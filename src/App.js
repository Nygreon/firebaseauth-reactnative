import React, { Component } from 'react';
import { SafeAreaView } from 'react-native';
import { Header, Button, Spinner, CardSection } from './components/common';
import firebase from 'firebase';
import LoginForm from './components/LoginForm';

class App extends Component {
    state = { loggedIn: null };

    componentWillMount() {
        firebase.initializeApp({
            apiKey: 'AIzaSyDs74eHgyQ2okbJb6P-1Kw7SJ62MEDbOGU',
            authDomain: 'authentication-69af4.firebaseapp.com',
            databaseURL: 'https://authentication-69af4.firebaseio.com',
            storageBucket: 'authentication-69af4.appspot.com',
            messagingSenderId: '595770388792',
        });

        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({ loggedIn: true });
            } else {
                this.setState({ loggedIn: false });
            }
        });
    }

    renderContent() {
        switch (this.state.loggedIn) {
            case true:
                return (
                    <CardSection>
                        <Button onPress={() => firebase.auth().signOut()}>Log out</Button>
                    </CardSection>
                );
            case false:
                return <LoginForm />;
            default:
                return <Spinner size="large" />;
        }

    }

    render() {
        return (
            <SafeAreaView>
                <Header headerText="Authentication" />
                {this.renderContent()}
            </SafeAreaView>
        );
    }
}

export default App;